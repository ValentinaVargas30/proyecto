//GeoJSON: JSON que describe objetos geograficos
// puntos (point)
//lineas (linestring)
//Poligono (Polygon)
//Multiples lineas (MultilineString)
//Multiles poligonos (MultyPolygon)
//Coleccion geometrias (Geometry Collection)
//Entidad Geografica (Features): Objeto geografico = Metadata

//Se recomienda que un GeoJSON define features, no solo la geometria 
let sitio={
    "type": "FeatureCollection",
    "features": [
      {
        "type": "Feature",
        "properties": {
            "popupcontent": "Este es mi cultivo",
            "Description": "Esta es la descripcion de mi cutivo"
        },
        "geometry": {
          "type": "Polygon",
          "coordinates": [
            [
              [
                -74.15445864200592,
                4.629909449647802
              ],
              [
                -74.15421724319458,
                4.630211550183378
              ],
              [
                -74.15459543466568,
                4.63052701786159
              ],
              [
                -74.15482342243195,
                4.63025967238067
              ],
              [
                -74.15445864200592,
                4.629909449647802
              ]
            ]
          ]
        }
      }
    ]
  }
