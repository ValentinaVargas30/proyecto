// Crear un objeto (variable)
//L representa a la bibioteca leaflet
let miMapa = L.map('mapid'); 

//Determinar la vista inicial
miMapa.setView([4.630083,-74.154539], 13);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
miProveedor.addTo(miMapa);
 
//Para agregarlo a mi mapa 

let miGeoJson=L.geoJson(sitio);

miGeoJson.addTo(miMapa);


//Crear un objeto marcador
let miMarcador = L.marker([4.630083,-74.154539]);
miMarcador.addTo(miMapa);

miMarcador.bindPopup("<br>Nombre de la granja: Virtualgreenfarm.</br><br>Nombre agricultor: Angie Valentina Vargas Monroy.<br>Tipo de planta:Frijol<br>Nombre cientifico: Phaseolus vulgaris.<br>Fecha de plantacion: 2021/01/02.<br>Humedad relativa:15%.<br>Familia: Leguminosae.<br>Genero: Phaseolus.<br>Temperatura optima: 18°C-21°C.<br>PH del suelo: Entre 5,5 y 6,5.<br>Piso termico: Templado y calido.<br>Ubicacion:[4.630083,-74.154539].").openPopup();
miMarcador.addTo(miMapa);

let miMarcador2 = L.marker([4.690453,-74.147257]);
miMarcador.addTo(miMapa);

//JSON
let circle = L.circle([4.630083,-74.154539], {
    color: 'green',
    fillColor: 'green',
    fillOpacity: 0.5,
    radius: 10

});
circle.addTo(miMapa);